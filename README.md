# 人工智能作业

#### 介绍
这里存放着我的人工智能作业

#### 包含内容
1. [人工智能——知识图谱表示法——使用Echarts关系图展示《红楼梦》人物关系图谱
](https://www.cnblogs.com/wsgxg/p/14999957.html)
2. [人工智能——知识图谱表示法(添加人物关系搜索)——使用Echarts关系图展示《红楼梦》人物关系图谱
](https://www.cnblogs.com/wsgxg/p/15000004.html)
3. [人工智能——构建依存树——使用LTP分词
](https://www.cnblogs.com/wsgxg/p/15000080.html)
4. [人工智能——jieba分词示例
](https://www.cnblogs.com/wsgxg/p/15001241.html)
5. [人工智能——爬取金庸小说人物介绍
](https://www.cnblogs.com/wsgxg/p/15001545.html)
5. [人工智能——爬取金庸小说人物关系
](https://www.cnblogs.com/wsgxg/p/15001570.html)
